class 個人の点数 {
    constructor(点数) {
        this.国語 = 点数.国語;
        this.数学 = 点数.数学;
        this.英語 = 点数.英語;
    }

    合計() {
        return this.国語 + this.数学 + this.英語;
    }

    平均() {
        return this.合計() / 3;
    }


    成績() {
        return `
国語: ${this.国語}
数学: ${this.数学}
英語: ${this.英語}
合計: ${this.合計()}
平均: ${this.平均()}
`;
    }
}
const 一郎の点数　= new　個人の点数({国語:87, 数学:78, 英語:66 })
console.log(一郎の点数.成績());